<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo_text.svg" width="320" alt="Nest Logo" /></a>
</p>

[circleci-image]: https://img.shields.io/circleci/build/github/nestjs/nest/master?token=abc123def456
[circleci-url]: https://circleci.com/gh/nestjs/nest

  <p align="center">A progressive <a href="http://nodejs.org" target="_blank">Node.js</a> framework for building efficient and scalable server-side applications.</p>
    <p align="center">
<a href="https://www.npmjs.com/~nestjscore" target="_blank"><img src="https://img.shields.io/npm/v/@nestjs/core.svg" alt="NPM Version" /></a>
<a href="https://www.npmjs.com/~nestjscore" target="_blank"><img src="https://img.shields.io/npm/l/@nestjs/core.svg" alt="Package License" /></a>
<a href="https://www.npmjs.com/~nestjscore" target="_blank"><img src="https://img.shields.io/npm/dm/@nestjs/common.svg" alt="NPM Downloads" /></a>
<a href="https://circleci.com/gh/nestjs/nest" target="_blank"><img src="https://img.shields.io/circleci/build/github/nestjs/nest/master" alt="CircleCI" /></a>
<a href="https://coveralls.io/github/nestjs/nest?branch=master" target="_blank"><img src="https://coveralls.io/repos/github/nestjs/nest/badge.svg?branch=master#9" alt="Coverage" /></a>
<a href="https://discord.gg/G7Qnnhy" target="_blank"><img src="https://img.shields.io/badge/discord-online-brightgreen.svg" alt="Discord"/></a>
<a href="https://opencollective.com/nest#backer" target="_blank"><img src="https://opencollective.com/nest/backers/badge.svg" alt="Backers on Open Collective" /></a>
<a href="https://opencollective.com/nest#sponsor" target="_blank"><img src="https://opencollective.com/nest/sponsors/badge.svg" alt="Sponsors on Open Collective" /></a>
  <a href="https://paypal.me/kamilmysliwiec" target="_blank"><img src="https://img.shields.io/badge/Donate-PayPal-ff3f59.svg"/></a>
    <a href="https://opencollective.com/nest#sponsor"  target="_blank"><img src="https://img.shields.io/badge/Support%20us-Open%20Collective-41B883.svg" alt="Support us"></a>
  <a href="https://twitter.com/nestframework" target="_blank"><img src="https://img.shields.io/twitter/follow/nestframework.svg?style=social&label=Follow"></a>
</p>
  <!--[![Backers on Open Collective](https://opencollective.com/nest/backers/badge.svg)](https://opencollective.com/nest#backer)
  [![Sponsors on Open Collective](https://opencollective.com/nest/sponsors/badge.svg)](https://opencollective.com/nest#sponsor)-->

## Description

In this server app a single document from mongo db is update with the data providen from the API of [hn.algolia.com](https://hn.algolia.com/api/v1/search_by_date?query=nodejs) of the last notices related to node js caching the elements al returning then with a single endpoint, the news data are refreshed once hour with a CRON job, if server is restarted at the same last update hour, the data is not updated again

## Initialization
- This backend service requires a '.env' file to work in local, you can use the [env.example](./env.example) located in this folder to configurate the service env variables.

- Make sure you have the mongodb service running in your machine, you can install it with the following guide [Mongodb manual installation](https://docs.mongodb.com/manual/installation/)

## Endpoints
*  `GET /hints`
  - Returns: Array of node js news data
  ### Type per element: 
    id: string;
    story_title: string | null;
    title: string | null;
    url: string | null;
    story_url: string | null;
    author: string;
    story_id: string;
  

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# test coverage
$ npm run test:cov
```

## Running as docker
### Building docker image
- First of all you need to build the service docker image, you can build the image with the following command passing it respectives arg:

  Command
  ```bash
  docker build -t ${DOCKER_IMAGE_NAME} --build-arg DB_HOST=$DB_HOST --build-arg DB_PORT=$DB_PORT --build-arg DB_NAME=$DB_NAME TEST_DB_NAME=${TEST_DB_NAME} --build-arg HACKER_NEWS_API=$HACKER_NEWS_API .
  ```

  Example:
  ```bash
  docker build -t fullstack-backend --build-arg DB_HOST=localhost --build-arg DB_PORT=27017 --build-arg DB_NAME=backend-db TEST_DB_NAME=backend-db-test --build-arg HACKER_NEWS_API=https://hn.algolia.com/api/v1/search_by_date?query=nodejs .
  ```

### Running docker
- You can run the docker easyly with the following command:

  Command
  ```bash
  docker run --name=${DOCKER_NAME} -p 0.0.0.0:4001:4000 ${DOCKER_IMAGE_NAME}
  ```

  Example:
  ```bash
  docker run --name="fullstack-backend" -p 0.0.0.0:4001:4000 fullstack-backend
  ```

## Support

Nest is an MIT-licensed open source project. It can grow thanks to the sponsors and support by the amazing backers. If you'd like to join them, please [read more here](https://docs.nestjs.com/support).

## Stay in touch

- Author - [Kamil Myśliwiec](https://kamilmysliwiec.com)
- Website - [https://nestjs.com](https://nestjs.com/)
- Twitter - [@nestframework](https://twitter.com/nestframework)

## License

Nest is [MIT licensed](LICENSE).
