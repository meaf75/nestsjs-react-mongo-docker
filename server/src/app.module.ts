import { HintsModule } from './infrastructure/hints/hints.module';
import { InitializeHackerNewsModule } from './infrastructure/initialize-hacker-news/initializehackernews.module';
import { TasksModule } from './tasks/tasks.module';
import { ConfigModule } from './config/config.module';
import { HttpModule, Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { DatabaseModule } from './infrastructure/database/database.module';
import { ScheduleModule } from '@nestjs/schedule';

@Module({
  imports: [
    HttpModule,
    ScheduleModule.forRoot(),
    TasksModule,
    ConfigModule,
    DatabaseModule,
    InitializeHackerNewsModule,
    HintsModule,
  ],
  controllers: [AppController],
  providers: [
    AppService
  ],
})
export class AppModule { }
