import { Prop, Schema, SchemaFactory, ModelDefinition } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type HintsDocument = HintsSchema & Document;

export interface IHintElement {
  id: string;
  story_title: string | null;
  title: string | null;
  url: string | null;
  story_url: string | null;
  author: string;
  story_id: string;
}

@Schema()
export class HintsSchema {
  @Prop()
  hits: IHintElement[]

  @Prop()
  updateTime: number = -1;
}

export const Hitschema = SchemaFactory.createForClass(HintsSchema);

export const HitsDocumentName = 'Hints';

export const HitsModelDefinition: ModelDefinition = {
  name: HitsDocumentName,
  schema: Hitschema
}
