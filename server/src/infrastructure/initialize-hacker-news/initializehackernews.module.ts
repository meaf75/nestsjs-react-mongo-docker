import { HttpModule, Module } from '@nestjs/common';
import { ConfigModule } from '../../config/config.module';
import { HintsModule } from '../hints/hints.module';
import { InitializehackernewsController } from './initializehackernews.controller';
import { InitializeHackerNewsService } from './initializehackernews.service';

@Module({
    imports: [
        HttpModule,
        ConfigModule,
        HintsModule
    ],
    controllers: [
        InitializehackernewsController
    ],
    providers: [
        InitializeHackerNewsService
    ],
    exports: [InitializeHackerNewsService]
})
export class InitializeHackerNewsModule {
    constructor(private initializeHackerNewsService : InitializeHackerNewsService){
        // Initialize db & cached elements on start module
        this.initializeHackerNewsService.CheckForUpdates();
    }
}
