import { Controller } from '@nestjs/common';
import { InitializeHackerNewsService } from './initializehackernews.service';

@Controller('InitializehackernewsController')
export class InitializehackernewsController {
    constructor(private readonly initializeHackerNewsService: InitializeHackerNewsService) {}

    CheckForUpdates = () => {        
        return this.initializeHackerNewsService.CheckForUpdates();
    }
}
