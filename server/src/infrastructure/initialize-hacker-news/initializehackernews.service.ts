import { HttpService, Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '../../config/config.service';
import { SetCachedHints } from '../../constants';
import { HackerNewsResponse } from '../../interfaces/HackerNewsResponse.interface';
import { HintsService } from '../hints/services/hints.service';

@Injectable()
export class InitializeHackerNewsService {
    constructor(private httpService: HttpService, private config: ConfigService,private hintsService: HintsService) {}

    /**
     * Check and update hints data,
     * Called on start the server or on once hour
     */
    CheckForUpdates = async () => {
        
        let currentHint = await this.hintsService.getCurrentHints();
        
        if(currentHint){            
            // Check for hits update if the last update hour & day are different from current hour & day
            const updateDate = new Date(currentHint.updateTime);
            updateDate.setMinutes(0,0,0);   // Remove min,sec,ms to compare with the current rest of the date 

            const currDate = new Date();
            currDate.setMinutes(0,0,0);

            if(+updateDate == +currDate){
                Logger.log('No need to update')
                SetCachedHints(currentHint.hits);
                // Same date,hour, do not need to update hits
                return currentHint;
            }
        }
        
        /** Response given from the hacker news api */
        const newHintsResponse = await this.httpService.get(this.config.envConfig.HACKER_NEWS_API).toPromise();
        const hintsData : HackerNewsResponse = newHintsResponse.data;
                
        // Update hints
        currentHint = await this.hintsService.setHints(hintsData.hits);

        Logger.log('Database hints was updated ♻♻ 👌👌');

        return currentHint;
    }
}
