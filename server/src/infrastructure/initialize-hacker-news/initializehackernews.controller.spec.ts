import { HttpModule, HttpService } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { ConfigModule } from '../../config/config.module';
import { DatabaseModule } from '../database/database.module';
import { InitializehackernewsController } from './initializehackernews.controller';
import { InitializeHackerNewsModule } from './initializehackernews.module';
import { InitializeHackerNewsService } from './initializehackernews.service';

describe('Initialize hacker news module', () => {

  let controller: InitializehackernewsController;
  let service: InitializeHackerNewsService;

  jest.useFakeTimers();

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [InitializeHackerNewsModule,HttpModule,ConfigModule,DatabaseModule],
      controllers:[InitializehackernewsController],
      providers: [
        {
          provide: InitializeHackerNewsService,
          useValue: {}
        }
      ]
    }).compile();
    
    controller = module.get<InitializehackernewsController>(InitializehackernewsController);    
    service = module.get<InitializeHackerNewsService>(InitializeHackerNewsService);    
  })

  it('should be defined', () => {
    expect(controller).toBeDefined()
  })

  it('should be defined service', () => {
    expect(service).toBeDefined()
  })

  it('Check hints are not empty', async () => {
    const cachedHints = await controller.CheckForUpdates();

    expect(cachedHints.hits.length).toBeGreaterThan(0); // Given hints from the API are always more than 2
  })
});
