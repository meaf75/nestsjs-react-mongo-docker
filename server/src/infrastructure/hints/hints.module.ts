import { HintsService } from './services/hints.service';
import { HintsController } from './controller/hints.controller';
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { HitsModelDefinition } from '../../schemas/hints.schema';
import { cachedHints } from '../../constants';

@Module({
    imports: [MongooseModule.forFeature([HitsModelDefinition])],
    controllers: [HintsController,],
    providers: [HintsService,{provide: 'cachedHints',useValue: cachedHints}],
    exports: [HintsService]
})
export class HintsModule { }
