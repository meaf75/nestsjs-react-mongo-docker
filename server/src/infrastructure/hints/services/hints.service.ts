import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { SetCachedHints } from '../../../constants';
import { HintsDocument, HitsDocumentName, IHintElement } from '../../../schemas/hints.schema';

@Injectable()
export class HintsService {
    constructor(@InjectModel(HitsDocumentName) private readonly hintsModel: Model<HintsDocument>) {}

    // Get all hints
    async getCurrentHints(): Promise<HintsDocument> | null {
        // I not need to find all the documents, with the first one with the reference of the current hour are required
        const hints = await this.hintsModel.findOne();
        return hints;
    }    


    async setHints(newHints: IHintElement[]): Promise<HintsDocument> {
        let currentHint = await this.getCurrentHints();
        const currDate = Date.now();

        // Generate elements id, some given element from the api comes with a null story_id
        newHints.forEach((hintElement,i) => {
            hintElement.id = `${currDate}-${i}`;
        });
        
        if(!currentHint){
            // Create/initialize object database
            const createdObject = await this.hintsModel.create({hits: newHints, updateTime: currDate});
            currentHint = createdObject;
        }else{
            // Update the first list of hints
            currentHint.hits = newHints;
            currentHint.updateTime = currDate;
            await currentHint.save();
        }

        // Update cached hits, to avoid make db request
        SetCachedHints(newHints);

        return currentHint;
    }    
}
