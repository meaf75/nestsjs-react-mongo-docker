import { HttpModule, HttpService } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { IHintElement } from 'src/schemas/hints.schema';
import { ConfigModule } from '../../config/config.module';
import { DatabaseModule } from '../database/database.module';
import { HintsModule } from './hints.module';
import { HintsService } from './services/hints.service';

describe('Infrastructure hints service', () => {

  let service: HintsService;

  jest.useFakeTimers();

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [HintsModule,HttpModule,ConfigModule,DatabaseModule],
      providers: [
        {
          provide: HintsService,
          useValue: {}
        }
      ]
    }).compile();
    
    service = module.get<HintsService>(HintsService);    
  })

  it('should be defined service', () => {
    expect(service).toBeDefined()
  })

  it('Check are saved', async () => {
    const newHints : IHintElement[] = [
      {id: '1', author: 'meaf1', story_id: '0', story_title: null, story_url: 'google.com', title: 'title_1',url: null},
      {id: '2', author: 'meaf2', story_id: '2', story_title: null, story_url: 'google.com', title: 'title_2',url: 'google.com'},
      {id: '3', author: 'meaf3', story_id: '3', story_title: null, story_url: 'google.com', title: 'title_3',url: null},
      {id: '4', author: 'meaf4', story_id: '4', story_title: null, story_url: 'google.com', title: 'title_4',url: 'gugul.com'},
      {id: '5', author: 'meaf5', story_id: '5', story_title: null, story_url: 'google.com', title: 'title_5',url: null},
    ]

    const savedHints = await service.setHints(newHints);
    
    expect(savedHints.hits.length).toBe(newHints.length);
  })
});
