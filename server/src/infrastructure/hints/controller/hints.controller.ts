import { Controller, Get, HttpStatus, Res } from '@nestjs/common';
import { cachedHints } from '../../../constants';
import { HintsService } from '../services/hints.service';

@Controller('hints')
export class HintsController { 

    constructor(private hintsService: HintsService) { }

    @Get('/')
    async getProducts(@Res() res) {
        
        // const products = await this.hintsService.getCurrentHints();
        
        // There is no need to use db request, the only data that is used is refreshed each hour
        // And on refresh cached hinds are updated
        return res.status(HttpStatus.OK).json(cachedHints);
    }
}
