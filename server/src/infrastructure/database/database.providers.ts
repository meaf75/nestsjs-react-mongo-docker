import { MongooseModule } from '@nestjs/mongoose';
import { ConfigModule } from '../../config/config.module';
import { ConfigService } from '../../config/config.service';

export const databaseProviders = [
	MongooseModule.forRootAsync({
		imports: [ConfigModule],
		useFactory: async (config: ConfigService) => ({
			uri: config.DB_URI,
			useNewUrlParser: true,			
			dbName: process.env.NODE_ENV === 'test' ? config.envConfig.TEST_DB_NAME : config.envConfig.DB_NAME
		}),
		inject: [ConfigService],		
	}),
];