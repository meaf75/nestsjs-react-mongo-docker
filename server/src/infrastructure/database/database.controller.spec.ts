import { Test, TestingModule } from '@nestjs/testing';
import { ConfigModule } from '../../config/config.module';
import { DatabaseController } from './database.controller';
import { DatabaseModule } from './database.module';

describe('Database Controller', () => {

  let controller: DatabaseController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [DatabaseModule],
    }).compile();
    
    controller = module.get<DatabaseController>(DatabaseController);
  })

  it('should be defined', () => {
    expect(controller).toBeDefined()
  })
});
