import { Module } from '@nestjs/common';
import { DatabaseController } from './database.controller';
import { databaseProviders } from './database.providers';

@Module({
    imports: [...databaseProviders],
    exports: [...databaseProviders],
    controllers: [DatabaseController]
})
export class DatabaseModule { }
