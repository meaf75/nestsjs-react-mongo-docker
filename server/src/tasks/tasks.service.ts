
import { Injectable, Logger } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import { InitializeHackerNewsService } from '../infrastructure/initialize-hacker-news/initializehackernews.service';

@Injectable()
export class TasksService {
  private readonly logger = new Logger(TasksService.name);

  constructor(private hintsService: InitializeHackerNewsService){}

  @Cron('0 * * * *')
  async handleCron() {
    // Update hints data each hour
    Logger.log('🕗 New hour! time to update hints')
    this.hintsService.CheckForUpdates();
  }
}