import { TasksService } from './tasks.service';
import { Module } from '@nestjs/common';
import { InitializeHackerNewsModule } from '../infrastructure/initialize-hacker-news/initializehackernews.module';

@Module({
    imports: [InitializeHackerNewsModule],
    controllers: [],
    providers: [TasksService],
})
export class TasksModule { }
