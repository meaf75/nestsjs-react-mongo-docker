import { Logger } from "@nestjs/common";
import { IHintElement } from "./schemas/hints.schema";

export const APP_PORT = process.env.PORT || 4000;

/** Reference of the node js news from the endpoint */
export let cachedHints : IHintElement[] = [];

export const SetCachedHints = (_cachedHints : IHintElement[]) => {
    Logger.log("Cached hints were setted");
    cachedHints = _cachedHints;
}