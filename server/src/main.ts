import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { APP_PORT } from './constants';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.enableCors();

  await app.listen(APP_PORT);

  console.log(`\n◘◘◘◘◘◘◘ App running at ${APP_PORT} ◘◘◘◘◘◘◘ \n`);  
}
bootstrap();
