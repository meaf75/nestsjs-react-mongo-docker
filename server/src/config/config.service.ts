import { Injectable } from '@nestjs/common';
import * as dotenv from 'dotenv';
import * as fs from 'fs';

export interface IConfig {
    NODE_ENV?: string;
    DB_HOST: string;
    DB_PORT: string;
    DB_NAME: string;
    TEST_DB_NAME: string
    HACKER_NEWS_API: string;
}

@Injectable()
export class ConfigService {

    envConfig: IConfig;
    DB_URI: string;

    constructor() {
        this.FillEnvVariables();
    }

    FillEnvVariables = () => {
        const seriousCloudEnviroments = ['production', 'staging']

        if (seriousCloudEnviroments.includes(process.env.NODE_ENV)) {
            // use vm variables
            this.envConfig = {
                NODE_ENV: process.env.NODE_ENV,
                DB_HOST: process.env.DB_HOST,
                DB_PORT: process.env.DB_PORT,
                DB_NAME: process.env.DB_NAME,
                TEST_DB_NAME: process.env.TEST_DB_NAME,
                HACKER_NEWS_API: process.env.HACKER_NEWS_API,
            };
        } else {
            // use user defined variables
            // const envFilePath = path.com
            this.envConfig = dotenv.parse(fs.readFileSync('.env')) as any;
        }

        this.DB_URI = `mongodb://${this.envConfig.DB_HOST}:${this.envConfig.DB_PORT}`;
        
    }
}
