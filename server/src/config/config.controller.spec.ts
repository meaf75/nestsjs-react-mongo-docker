import { Test, TestingModule } from '@nestjs/testing';
import { ConfigController } from './config.controller';
import { ConfigModule } from './config.module';
import { ConfigService, IConfig } from './config.service';
import * as dotenv from 'dotenv';
import * as fs from 'fs';

describe('Config Controller', () => {

  let controller: ConfigController;
  let service: ConfigService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [ConfigModule],
      providers: [ConfigService,ConfigController]
    }).compile();
    
    controller = module.get<ConfigController>(ConfigController);
    service = module.get<ConfigService>(ConfigService);
  })

  it('should be defined', () => {
    expect(service).toBeDefined()
  })

  describe('Test Env variables', () => {

    if(process.env.NODE_ENV == 'local'){
      it('Test in local, variables assignations', () => {
        service.FillEnvVariables();
  
        const localEnvVariables = dotenv.parse(fs.readFileSync('.env')) as any;
  
        expect(service.DB_URI).toBeTruthy();
        expect(service.envConfig).toMatchObject(localEnvVariables)
      })
    }

    it('Test in development/production, variables assignations', () => {      
      service.FillEnvVariables();
      expect(service.DB_URI).toBeTruthy();
    })
  })
  
});
