import { IHintElement } from "../schemas/hints.schema";

export interface HackerNewsResponse {
    hits: IHintElement[],
    nbHits: number,
    page: number,
    nbPages: number,
    hitsPerPage: number,
    exhaustiveNbHits: boolean,
    query: string,
    params: string,
    processingTimeMS: number
}

