# NESTSJS - REACT - MONGO - DOCKER
This is a working small example full stack project: 
- Dockerized working backend server made in nest js using mongodb 
- Dockerized working frontent client made in react js
- Full docker-compose up, backend-client-mongodb

# Considerations
- Make sure you have the mongodb service running in your machine, you can install it with the following guide [Mongodb manual installation](https://docs.mongodb.com/manual/installation/)
- Make sure you have the docker service running in your machine, you can install it with the following guide [Docker manual installation](https://docs.docker.com/get-docker/)
- Make sure you have docker-compose installed your machine, you can install it with the following guide [Docker-compose manual installation](https://docs.docker.com/compose/install/)

# Docker compose initialization
docker volume create --name=fullStackBackendDB

# Run docker compose
docker-compose up --build --d

# Docker compose considerations
- In the client the enviroment variables must start with "REACT_APP_" so thats why the client env variable of the host is named as "REACT_APP_API_ENDPOINT"
- At the backend service 

# Stack services information
- You can find more information about the backend service at [BACKEND README.md](./server/README.md)

- You can find more information about the client service at [CLIENT README.md](./client/README.md)