import React, { useEffect, useState } from 'react';
import { DELETED_ELEMENTS_KEY } from '../../../constants/DELETED_ELEMENTS_KEY';
import { IDictionary } from '../../../interfaces/IDictionary.interface';
import { IHintElement } from '../../../interfaces/IHintElement.interface';
import { GetHintsRequest } from '../../../services/hints.service';
import { handle } from '../../../utils/PromiseHandler';
import { FeedNewElement } from '../../molecules/HackerNewsScreen/FeedNewElement';
import './HackerNewsScreen.css'

export const HackerNewsScreen = () => {

    const [hintsData,setHintsData] = useState<IHintElement[]>([]);
    const [dataLoaded,setDataLoaded] = useState(false);
    const [hintsElements,setHintsElements] = useState<JSX.Element[] | null>(null);
    const [errorOnGetHints,setErrorOnGetHints] = useState(false);

    //#region Callbacks
    const GetDeletedElements = () : IDictionary<string> => {
        const deletedElementsJson : string | null = localStorage.getItem(DELETED_ELEMENTS_KEY);
        let deletedElements = {};

        if(!deletedElementsJson)
            deletedElements = {};   // Initialice element
        else
            deletedElements = JSON.parse(deletedElementsJson);  // Restore data

        return deletedElements;
    }

    const GetNews = async () => {
        const [hintsResponse,err] = await handle(GetHintsRequest());

        if(err){
            console.error('An error ocurred trying to get the hints',err);
            setErrorOnGetHints(true);
            return;
        }

        // Remove deleted elements
        const deletedElements = GetDeletedElements();
        const hintsDataInfo = hintsResponse.data as IHintElement[];
        const fixedHindsNews = hintsDataInfo.filter(h => !deletedElements[h.id]).sort((a,b) => {
            const dateA = new Date(a.created_at);
            const dateB = new Date(b.created_at);

            return +dateB - +dateA
        });

        setHintsData(fixedHindsNews);
        setDataLoaded(true);
    }

    const DeleteElement = (hintElementId: string) => {
        // Add id reference of the deleted element
        
        // I see no reason to delete the element from the server side based on the data is updated each hour
        // so i just store a local refence of the hint element id and filter them from the client
        let deletedElements = GetDeletedElements();
        deletedElements[hintElementId] = hintElementId;

        localStorage.setItem(DELETED_ELEMENTS_KEY,JSON.stringify(deletedElements));
        

        // Remove element from the local data
        const newHints = hintsData.filter(h => h.id !== hintElementId);
        setHintsData(newHints);
        
    }
    //#endregion

    //#region Hooks
    useEffect(() => {
        GetNews();
    },[])

    useEffect(() => {
        // Generate hints jsx elements
        let newHintsElements : JSX.Element[] = [];

        hintsData.forEach((hintData,i) => {

            const title = hintData.title ?? hintData.story_title ?? '';

            if(!title){
                // Skip element if does not contain a valid title
                console.log('skipping elment');                
                return;
            }

            // Build & reference jsx element
            const newElment = <FeedNewElement hintData={hintData} key={`elm-${i}`} onClickDeleteElement={DeleteElement}/>;            
            newHintsElements.push(newElment);
        });

        setHintsElements(newHintsElements);
    },[hintsData])
    //#endregion

    // Display warning message if there is no data to be displayed
    const emptyNews = dataLoaded && hintsData.length === 0 ? (
        <h2>You've seen all new post from the last hour</h2>
    ) : null;

    return (
        <>
            {/* Header */}
            <div className='news-header'>
                <h1 className='title'>HN Feed</h1>
                <a>{'We 💖 hacker news!'}</a>
            </div>

            {errorOnGetHints ? <h2>Error on get hints</h2> : null }

            {/* Hints content */}
            {hintsElements}

            {emptyNews}
        </>
    );
}
