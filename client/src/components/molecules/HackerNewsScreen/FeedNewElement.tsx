import { useEffect, useState } from "react";
import { MonthNames } from "../../../constants/MonthNames";
import { TRASH_IMG_SRC } from "../../../constants/StaticImages"
import { IHintElement } from "../../../interfaces/IHintElement.interface"
import { GetTimeLabel } from "../../../utils/GetTimeLabel";
import './FeedNewElement.css'

export interface IFeedNewElementProps {
    hintData: IHintElement,
    onClickDeleteElement: (hintElementId: string) => void
}

export const FeedNewElement = (props: IFeedNewElementProps) => {

    const [postPublishText,setPostPublishText] = useState('');

    const title = props.hintData.story_title ?? props.hintData.title ?? '';

    //#region Callbacks
    const onPressDelete = (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
        e.stopPropagation();    // Avoid open post link
        props.onClickDeleteElement(props.hintData.id);
    }

    const OpenLinkElement = () => {
        const url = props.hintData.story_url ?? props.hintData.url;

        if(!url){
            console.error('Empty url');
            return
        }


        window.open(url, "_blank") //to open new page
    }
    //#endregion

    useEffect(()=>{
        //#region Get & fix post date
        const currentDate = new Date();
        const publishDate = new Date(props.hintData.created_at);

        const diffDays = Math.abs(currentDate.getDate() - publishDate.getDate());
        
        let publishDateLabel = '';

        if(diffDays === 0){
            // Fix for current date HH:DD prefix
            publishDateLabel = GetTimeLabel(publishDate);
        }else if(diffDays === 1)
            publishDateLabel = 'Yesterday';
        else 
            publishDateLabel = `${MonthNames[publishDate.getMonth()]} ${publishDate.getDate()}`;
        
        setPostPublishText(publishDateLabel)
        //#endregion
    },[])

    return (
        <div className='container' onClick={OpenLinkElement}>
            {/* News title & autor */}
            <div className='title-container'>
                <span>{title}</span>
                
                <div className='autor-label-container'>
                    <span>- {props.hintData.author} -</span>
                </div>
            </div>

            {/* Publish date & delete button */}
            <div className='news-publish-info-container'>
                <p className='hour-label'>{postPublishText}</p>
                
                <div className='trash-img-container' onClick={onPressDelete}>
                    {/* unicode */}
                    <img className='trash-img' alt='trash' src={TRASH_IMG_SRC}/>
                </div>
            </div>
        </div>
    )
}