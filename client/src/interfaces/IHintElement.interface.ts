export interface IHintElement {
    id: string;
    title: string | null;
    story_title: string | null;
    url: string | null;
    story_url: string | null;
    author: string;
    story_id: string;
    created_at: string;
}