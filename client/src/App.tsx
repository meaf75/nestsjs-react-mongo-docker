import React from 'react';
import logo from './logo.svg';
import './App.css';
import { HackerNewsScreen } from './components/organisms/HackerNewsScreen/HackerNewsScreen';

function App() {
  return (
    <div className="App">
      <HackerNewsScreen/>
    </div>
  );
}

export default App;
