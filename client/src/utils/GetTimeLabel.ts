/** Fix for current date HH:DD prefix */
export const GetTimeLabel = (date: Date) => {
    const hour = date.getHours() % 12;
    const minutes = date.getMinutes();

    const minutesLabel = minutes > 9 ? minutes : '0' + minutes;
    const hoursLabel = hour > 9 ? hour : '0' + hour;
    const prefixLabel = hour ? 'pm' : 'am';

    return `${hoursLabel}:${minutesLabel} ${prefixLabel}`;
}
