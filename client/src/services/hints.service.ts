import Axios from "axios"
import { API_ENDPOINT } from "../constants/ApiEndPoint"

/**
 * Web request to get the hits to be displayed from the 
 */
export const GetHintsRequest = () => {
    return Axios.get(`${API_ENDPOINT}/hints`)
}